package com.livecricket.krishna.activity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.livecricket.krishna.R;
import com.livecricket.krishna.network.ApiClient;
import com.livecricket.krishna.network.ApiInterface;
import com.livecricket.krishna.premissionManager.PermissionsManager;
import com.livecricket.krishna.premissionManager.PermissionsResultAction;
import com.livecricket.krishna.responce.LoginResonce;
import com.livecricket.krishna.utils.AppConfig;
import com.livecricket.krishna.utils.PreferenceHelper;
import com.livecricket.krishna.utils.Progress;
import com.livecricket.krishna.utils.Utils;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";

    @BindView(R.id.tb_signin)
    Toolbar tbSignin;
    @BindView(R.id.et_username)
    EditText etUsername;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.btn_signin)
    Button btnSignin;
    @BindView(R.id.txt_signup)
    TextView txtSignup;
    @BindView(R.id.txt_forgotpwd)
    TextView txtForgotpwd;

    private Utils utils;
    private Progress progress;
    private PreferenceHelper helper;
    private String android_id ="";

    private boolean mGranted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (new PreferenceHelper(this, AppConfig.PREFERENCE.PREF_FILE)
                .LoadBooleanPref(AppConfig.PREFERENCE.USER_LOGIN, false)) {
            startActivity(new Intent(this, HomeActivity.class));
            finish();
            return;
        }
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        setSupportActionBar(tbSignin);

        utils = new Utils(this);
        progress = new Progress(this);
        helper = new PreferenceHelper(this, AppConfig.PREFERENCE.PREF_FILE);

        askPermissions();

        android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);



    }


    private void signin() {


        String email = etUsername.getText().toString();
        String password = etPassword.getText().toString();


        if (email.isEmpty()) {

            etUsername.setError("Enter the UserName");
            utils.requestFocus(etUsername);
            return;
        }
        if (password.isEmpty()) {

            etPassword.setError("Enter the Password");
            utils.requestFocus(etPassword);
            return;
        }

//
//        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
//        finish();

//        Log.e(TAG, "signin: "+email + " "+password );
        loginuser(email, password,android_id);


    }

    private boolean validateuserId() {
        String email = etUsername.getText().toString().trim();
        if (email.isEmpty() || !Utils.isValidEmail(email)) {
            etUsername.setError("Please Enter Valid Email ID");
            utils.requestFocus(etUsername);
            return false;
        } else {
            return true;
        }
    }

    private void loginuser(String email, String password, String deviceid) {

        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();
        utils.hideKeyboard();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<LoginResonce> loginResponceCall = apiService.LOGIN_RESPONCE_CALL(email, password,deviceid);

        loginResponceCall.enqueue(new Callback<LoginResonce>() {
            @Override
            public void onResponse(@NonNull Call<LoginResonce> call, @NonNull Response<LoginResonce> response) {
                progress.hideDialog();
                LoginResonce loginResponce = response.body();


                if (response.code() == AppConfig.URL.SUCCESS) {
                    if (!response.body().isError()) {

                        if(loginResponce.getUserdetails().getStatus().equalsIgnoreCase("0")){
                            Toast.makeText(LoginActivity.this, "Wait for admin approval. Please contact customer care for more information.Thank you.", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if(loginResponce.getUserdetails().getInOut().equalsIgnoreCase("1")){
                            Toast.makeText(LoginActivity.this, "You are already logged-in from this User, please logout from your old device to login into new device.", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        Toast.makeText(LoginActivity.this, loginResponce.getMessage(), Toast.LENGTH_SHORT).show();
                        helper.initPref();
                        helper.SaveStringPref(AppConfig.PREFERENCE.USER_NAME,loginResponce.getUserdetails().getUname());
                        helper.SaveStringPref(AppConfig.PREFERENCE.FULL_NAME,loginResponce.getUserdetails().getName());
                        helper.SaveStringPref(AppConfig.PREFERENCE.USER_ID,loginResponce.getUserdetails().getId());
                        helper.SaveStringPref(AppConfig.PREFERENCE.USER_EMAIL,loginResponce.getUserdetails().getEmail());
                        helper.SaveStringPref(AppConfig.PREFERENCE.USER_NO,loginResponce.getUserdetails().getContactNumber());
                        helper.SaveStringPref(AppConfig.PREFERENCE.USER_AUTHKEY,loginResponce.getUserdetails().getAuthkey());
                        helper.SaveStringPref(AppConfig.PREFERENCE.DEVICE_ID,loginResponce.getUserdetails().getDeviceid());
                        helper.SaveStringPref(AppConfig.PREFERENCE.SUBEND_DATE,loginResponce.getUserdetails().getEnddate());
                        helper.SaveBooleanPref(AppConfig.PREFERENCE.USER_LOGIN, true);
                        helper.ApplyPref();
                        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                        finish();


                    } else {

                        Log.e(TAG, "onResponse: "+response.message() );
                        Toast.makeText(LoginActivity.this, loginResponce.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                } else {
                    Toast.makeText(LoginActivity.this, getString(R.string.msg_unexpected_error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<LoginResonce> call, @NonNull Throwable t) {
                progress.hideDialog();
                Log.e(TAG, "onFailure: "+t.getMessage() );
                Log.e(TAG, "onFailure: "+t.getCause() );
                t.printStackTrace();
                Toast.makeText(LoginActivity.this, getString(R.string.msg_internet_conn), Toast.LENGTH_SHORT).show();            }
        });

    }


    @OnClick({R.id.txt_forgotpwd, R.id.btn_signin, R.id.txt_signup})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txt_forgotpwd:
                startActivity(new Intent(LoginActivity.this, LoginActivity.class));
                finish();
                break;
            case R.id.btn_signin:
                signin();
                break;
            case R.id.txt_signup:
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
                finish();
                break;
        }
    }

    private void askPermissions() {
        PermissionsManager.getInstance().requestAllManifestPermissionsIfNecessary(LoginActivity.this, new PermissionsResultAction() {
            @Override
            public void onGranted() {

                mGranted = true;
            }

            @Override
            public void onDenied(String permission) {
                String message = String.format(Locale.getDefault(), "You have to Accept All the permissions.", permission);
//                Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        PermissionsManager.getInstance().notifyPermissionsChange(permissions, grantResults);
    }
}
