//package com.livecricket.webInr.activity;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.Toolbar;
//import android.util.Log;
//import android.view.View;
//import android.widget.Toast;
//
//import com.livecricket.webInr.R;
//import com.livecricket.webInr.utils.Constants;
//
//import us.zoom.sdk.MeetingError;
//import us.zoom.sdk.MeetingService;
//import us.zoom.sdk.MeetingServiceListener;
//import us.zoom.sdk.MeetingStatus;
//import us.zoom.sdk.StartMeetingOptions;
//import us.zoom.sdk.StartMeetingParams4APIUser;
//import us.zoom.sdk.ZoomError;
//import us.zoom.sdk.ZoomSDK;
//import us.zoom.sdk.ZoomSDKInitializeListener;
//
//
//public class MeetingActivity extends AppCompatActivity implements Constants, MeetingServiceListener, ZoomSDKInitializeListener {
//
//    private static final String TAG = "MeetingActivity";
//    private String meetingid = "";
//    private final static String DISPLAY_NAME = "My Live Cricket";
//
//    public final static String ACTION_RETURN_FROM_MEETING = "com.livecricket.webInr.activity.action.ReturnFromMeeting";
//    public final static String EXTRA_TAB_ID = "tabId";
//    public final static int TAB_WELCOME = 1;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_meeting);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//
//
//
//        ZoomSDK zoomSDK = ZoomSDK.getInstance();
//
//        if (savedInstanceState == null) {
//            zoomSDK.initialize(this, APP_KEY, APP_SECRET, WEB_DOMAIN, this);
//            Log.e(TAG, "initZoomSDK: " + "success");
//        }
//
//        if (zoomSDK.isInitialized()) {
//            registerMeetingServiceListener();
//            if(!meetingid.isEmpty()){
//
//
//                if(!zoomSDK.isInitialized()) {
//                    Toast.makeText(this, "ZoomSDK has not been initialized successfully", Toast.LENGTH_LONG).show();
//                    return;
//                }
//
//                MeetingService meetingService = zoomSDK.getMeetingService();
//                if(meetingService == null)
//                    return;
//
//                if(meetingService.getMeetingStatus() == MeetingStatus.MEETING_STATUS_IDLE){
//
//                    startMeeting();
//
//                } else {
//
//                    meetingService.returnToMeeting(this);
//                }
//
//                overridePendingTransition(0, 0);
//
//
//
//            }
//        }
//
//    }
//
//    @Override
//    protected void onNewIntent(Intent intent) {
//        super.onNewIntent(intent);
//
//        // disable animation
//        overridePendingTransition(0,0);
//
//        String action = intent.getAction();
//
//        if(ACTION_RETURN_FROM_MEETING.equals(action)) {
//            int tabId = intent.getIntExtra(EXTRA_TAB_ID, TAB_WELCOME);
////            selectTab(tabId);
//        }
//    }
//
//
//    private void registerMeetingServiceListener() {
//        ZoomSDK zoomSDK = ZoomSDK.getInstance();
//        MeetingService meetingService = zoomSDK.getMeetingService();
//        if(meetingService != null) {
//            meetingService.addListener(this);
//        }
//    }
//
//    @Override
//    public void onMeetingStatusChanged(MeetingStatus meetingStatus, int errorCode,
//                                       int internalErrorCode) {
//
//        if(meetingStatus == meetingStatus.MEETING_STATUS_FAILED && errorCode == MeetingError.MEETING_ERROR_CLIENT_INCOMPATIBLE) {
//            Toast.makeText(this, "Version of ZoomSDK is too low!", Toast.LENGTH_LONG).show();
//        }
//
//        if(meetingStatus == MeetingStatus.MEETING_STATUS_IDLE || meetingStatus == MeetingStatus.MEETING_STATUS_FAILED) {
////            selectTab(TAB_WELCOME);
//        }
//    }
//
//    @Override
//    public void onZoomSDKInitializeResult(int errorCode, int internalErrorCode) {
//        Log.i(TAG, "onZoomSDKInitializeResult, errorCode=" + errorCode + ", internalErrorCode=" + internalErrorCode);
//
//        if(errorCode != ZoomError.ZOOM_ERROR_SUCCESS) {
//            Toast.makeText(this, "Failed to initialize Zoom SDK. Error: " + errorCode + ", internalErrorCode=" + internalErrorCode, Toast.LENGTH_LONG);
//        } else {
//            Toast.makeText(this, "Initialize Zoom SDK successfully.", Toast.LENGTH_LONG).show();
//
//            registerMeetingServiceListener();
//        }
//        Log.e(TAG, "onZoomSDKInitializeResult: " + errorCode + " " + internalErrorCode);
//    }
//
//    public void startMeeting() {
//
//        ZoomSDK zoomSDK = ZoomSDK.getInstance();
//
//        if(!zoomSDK.isInitialized()) {
//            Toast.makeText(this, "ZoomSDK has not been initialized successfully", Toast.LENGTH_LONG).show();
//            return;
//        }
//
////		if(MEETING_ID == null) {
////			Toast.makeText(this, "MEETING_ID in Constants can not be NULL", Toast.LENGTH_LONG).show();
////			return;
////		}
//
//        MeetingService meetingService = zoomSDK.getMeetingService();
//
//        StartMeetingOptions opts = new StartMeetingOptions();
//        opts.no_driving_mode = true;
////		opts.no_meeting_end_message = true;
//        opts.no_titlebar = true;
//        opts.no_bottom_toolbar = true;
//        opts.no_invite = true;
//
//        StartMeetingParams4APIUser params = new StartMeetingParams4APIUser();
//        params.userId = USER_ID;
//        params.zoomToken = ZOOM_TOKEN;
//        params.meetingNo = meetingid;
//
//        Log.e(TAG, "startMeeting: "+meetingid );
//        params.displayName = DISPLAY_NAME;
//
//        int ret = meetingService.startMeetingWithParams(this, params, opts);
//
//        Log.i(TAG, "onClickBtnStartMeeting, ret=" + ret);
//    }
//
//    @Override
//    protected void onDestroy() {
//        ZoomSDK zoomSDK = ZoomSDK.getInstance();
//
//        if(zoomSDK.isInitialized()) {
//            MeetingService meetingService = zoomSDK.getMeetingService();
//            meetingService.removeListener(this);
//        }
//
//        super.onDestroy();
//    }
//
//}
