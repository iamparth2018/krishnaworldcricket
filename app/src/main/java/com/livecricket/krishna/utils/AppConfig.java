package com.livecricket.krishna.utils;

/**
 * Created by android2 on 10/4/17.
 */

public class AppConfig {


    public static final class URL {
        public static final String BASE_GETIP = "http://livecricket.today/admin/api/";
        public static final String Login = "login";
        public static final String Register = "registration";;
        public static final String Webinrlist = "webinarlist";
        public static final String UpdateUser = "updateProfile";
        public static final String ChangePwd = "changePassword";
        public static final String Logout = "logout";

        public static final int SUCCESS = 200;


    }

    public static final class PREFERENCE {

        public static final String PREF_FILE = "mylivecricket";
        public static final String USER_ID = "Userid";
        public static final String USER_LOGIN = "userlogin";
        public static final String USER_AUTHKEY = "authkey";
        public static final String DEVICE_ID = "deviceid";
        public static final String USER_NAME = "Username";
        public static final String FULL_NAME = "fullname";
        public static final String USER_EMAIL = "useremail";
        public static final String MEETINGID = "meetingid";
        public static final String MEETINGPWD = "meetingpwd";
        public static final String USER_NO = "meetingid";
        public static final String ZOOM_INTIALIZE = "zoominitialize";
        public static final String SUBEND_DATE = "subenddate";
        public static final String ZOOMKEY = "zoomkey";
        public static final String ZOOMSECRET = "zoomsecret";
        public static final String RANDOMUSERSTRING = "randomuserstring";



    }


}
