package com.livecricket.krishna.responce;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WebInrResponce {


    /**
     * enddate : 2019-01-31
     * error : false
     * message : success
     * status : 1
     * deviceid :
     * randstring : 3M_95530184_672_HLerjubc
     * AppKey : BVojJr4Psjxp1LLP8xh5If8Ytviy4qT8vAA3
     * SecKey : 0yq8zu9ze07fVLV9PQckrRqgtxXzbIZwCVwV
     * webdetails : [{"id":"69","webinarName":"test jay","fromDate":"03-05-2019 00:00","endDate":"05-05-2019 23:59","meetingId":"350200542","password":"123456","users":"449,1108","createDate":"2019-04-25 17:02:18"}]
     */

    @SerializedName("enddate")
    private String enddate;
    @SerializedName("error")
    private boolean error;
    @SerializedName("message")
    private String message;
    @SerializedName("status")
    private String status;
    @SerializedName("deviceid")
    private String deviceid;
    @SerializedName("randstring")
    private String randstring;
    @SerializedName("AppKey")
    private String AppKey;
    @SerializedName("SecKey")
    private String SecKey;
    @SerializedName("webdetails")
    private List<WebdetailsEntity> webdetails;

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getRandstring() {
        return randstring;
    }

    public void setRandstring(String randstring) {
        this.randstring = randstring;
    }

    public String getAppKey() {
        return AppKey;
    }

    public void setAppKey(String AppKey) {
        this.AppKey = AppKey;
    }

    public String getSecKey() {
        return SecKey;
    }

    public void setSecKey(String SecKey) {
        this.SecKey = SecKey;
    }

    public List<WebdetailsEntity> getWebdetails() {
        return webdetails;
    }

    public void setWebdetails(List<WebdetailsEntity> webdetails) {
        this.webdetails = webdetails;
    }

    public static class WebdetailsEntity {
        /**
         * id : 69
         * webinarName : test jay
         * fromDate : 03-05-2019 00:00
         * endDate : 05-05-2019 23:59
         * meetingId : 350200542
         * password : 123456
         * users : 449,1108
         * createDate : 2019-04-25 17:02:18
         */

        @SerializedName("id")
        private String id;
        @SerializedName("webinarName")
        private String webinarName;
        @SerializedName("fromDate")
        private String fromDate;
        @SerializedName("endDate")
        private String endDate;
        @SerializedName("meetingId")
        private String meetingId;
        @SerializedName("password")
        private String password;
        @SerializedName("users")
        private String users;
        @SerializedName("createDate")
        private String createDate;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getWebinarName() {
            return webinarName;
        }

        public void setWebinarName(String webinarName) {
            this.webinarName = webinarName;
        }

        public String getFromDate() {
            return fromDate;
        }

        public void setFromDate(String fromDate) {
            this.fromDate = fromDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public String getMeetingId() {
            return meetingId;
        }

        public void setMeetingId(String meetingId) {
            this.meetingId = meetingId;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getUsers() {
            return users;
        }

        public void setUsers(String users) {
            this.users = users;
        }

        public String getCreateDate() {
            return createDate;
        }

        public void setCreateDate(String createDate) {
            this.createDate = createDate;
        }
    }
}
