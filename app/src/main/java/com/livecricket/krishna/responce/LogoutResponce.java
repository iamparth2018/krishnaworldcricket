package com.livecricket.krishna.responce;

import com.google.gson.annotations.SerializedName;

public class LogoutResponce {


    /**
     * error : false
     * message : success
     * userdetails : {"id":"163","status":"1","name":"jaydeep","email":"","password":"123456","contactNumber":"","authkey":"jVVMua5PwbGikSy7euWj7XJn7NGCt2eu","enddate":"2018-10-31","createDate":"2018-10-04 11:40:26","uname":"jaydeep","deviceid":"21212121","in_out":"0"}
     */

    @SerializedName("error")
    private boolean error;
    @SerializedName("message")
    private String message;
    @SerializedName("userdetails")
    private UserdetailsEntity userdetails;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserdetailsEntity getUserdetails() {
        return userdetails;
    }

    public void setUserdetails(UserdetailsEntity userdetails) {
        this.userdetails = userdetails;
    }

    public static class UserdetailsEntity {
        /**
         * id : 163
         * status : 1
         * name : jaydeep
         * email :
         * password : 123456
         * contactNumber :
         * authkey : jVVMua5PwbGikSy7euWj7XJn7NGCt2eu
         * enddate : 2018-10-31
         * createDate : 2018-10-04 11:40:26
         * uname : jaydeep
         * deviceid : 21212121
         * in_out : 0
         */

        @SerializedName("id")
        private String id;
        @SerializedName("status")
        private String status;
        @SerializedName("name")
        private String name;
        @SerializedName("email")
        private String email;
        @SerializedName("password")
        private String password;
        @SerializedName("contactNumber")
        private String contactNumber;
        @SerializedName("authkey")
        private String authkey;
        @SerializedName("enddate")
        private String enddate;
        @SerializedName("createDate")
        private String createDate;
        @SerializedName("uname")
        private String uname;
        @SerializedName("deviceid")
        private String deviceid;
        @SerializedName("in_out")
        private String inOut;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getContactNumber() {
            return contactNumber;
        }

        public void setContactNumber(String contactNumber) {
            this.contactNumber = contactNumber;
        }

        public String getAuthkey() {
            return authkey;
        }

        public void setAuthkey(String authkey) {
            this.authkey = authkey;
        }

        public String getEnddate() {
            return enddate;
        }

        public void setEnddate(String enddate) {
            this.enddate = enddate;
        }

        public String getCreateDate() {
            return createDate;
        }

        public void setCreateDate(String createDate) {
            this.createDate = createDate;
        }

        public String getUname() {
            return uname;
        }

        public void setUname(String uname) {
            this.uname = uname;
        }

        public String getDeviceid() {
            return deviceid;
        }

        public void setDeviceid(String deviceid) {
            this.deviceid = deviceid;
        }

        public String getInOut() {
            return inOut;
        }

        public void setInOut(String inOut) {
            this.inOut = inOut;
        }
    }
}
