package com.livecricket.krishna.network;


import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.livecricket.krishna.utils.AppConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClient {

    private static Retrofit retrofit = null;


    private static OkHttpClient okHttpClient1 = new OkHttpClient().newBuilder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .readTimeout(10, TimeUnit.SECONDS)
            .addNetworkInterceptor(new StethoInterceptor())
            .build();


    public static Retrofit getClient() {
        if (retrofit == null) {
//            Gson gson = new GsonBuilder()
//                    .create();

            retrofit = new Retrofit.Builder()
                    .client(okHttpClient1)
                    .baseUrl(AppConfig.URL.BASE_GETIP)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

        }
        return retrofit;
    }


}
